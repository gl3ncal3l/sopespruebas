const mongoose = require("mongoose");
const schema = mongoose.Schema({
    name : String,
    location : String,
    age : String,
    infectedtype : String,
    state : String,
    way : String
});

module.exports = mongoose.model("Casos",schema);