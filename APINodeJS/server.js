const express = require("express");
const mongoose = require("mongoose");
const routes = require("./routes");

mongoose.connect("mongodb://mongo:27017/casocovid",{useNewUrlParser:true})
.then(() => {
    const app = express();
    app.use(express.json());
    app.use("/api",routes);

    app.listen(5000, () => {
        console.log("Server Node iniciado en puerto 5000");
    });
}).catch(err => console.error(err));





