const express = require("express");
const fs = require('fs');
const router = express.Router();
const Casos = require("./models/casos")

router.get("/",async(req,res) =>{
    res.send('{"msg":"ok"}');
});

router.get("/casos", async(req, res) => {
    const casos = await Casos.find();
    res.send(casos);
});

router.post("/casos",async(req, res) => {
    console.log(req.body)
    const casos = new Casos({
        name : req.body.name,
        location : req.body.location,
        age : req.body.age,
        infectedtype :req.body.infectedtype,
        state : req.body.state,
        way : req.body.way
    });
    await casos.save();
    res.send(casos);
})

router.get("/casos", async(req, res) => {
    const casos = await Casos.find();
    res.send(casos);
});


router.get("/get-memory-info", async(req, res) => {
    fs.readFile('/proc/memo_grupo12', 'utf8' , (err, data) => {
        if (err) {
          console.error(err)
          return
        }
        res.send(data);
    });
});

router.get("/get-cpu-info", async(req, res) => {
    fs.readFile('/proc/cpu_grupo12', 'utf8' , (err, data) => {
        if (err) {
          console.error(err)
          return
        }
        var info = data.replace(',]', ']');
        info = info.replace(',}', '}');
        res.send(info);
    });
});


module.exports = router;