#include <linux/proc_fs.h>
#include <linux/seq_file.h> 
#include <asm/uaccess.h> 
#include <linux/hugetlb.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>   
#include <linux/fs.h>

#define BUFSIZE  	150

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Modulo Memoria RAM [SO1] Primer Semestre 2021");
MODULE_AUTHOR("Grupo No 12");
struct sysinfo info_ram;

static int escribir_a_proc(struct seq_file * file_proc, void *v) {
	long mem_total;
	long mem_libre;
	long mem_buffer;
	long mem_shared;
	long mem_freehi;
    long mem_u;
    si_meminfo(&info_ram);
    mem_total 	= (info_ram.totalram * 4);
    mem_libre 	= (info_ram.freeram * 4);
	mem_buffer	= (info_ram.bufferram * 4);
	mem_shared	= (info_ram.sharedram * 4);
	mem_freehi	= (info_ram.freehigh * 4);
    mem_u = mem_total - (mem_libre + mem_shared  + mem_buffer * 2);
    seq_printf(file_proc, "{\n\"memoria_total\":%lu,\n", mem_total / 1024);
    seq_printf(file_proc, "\"memoria_libre\":%lu,\n", mem_libre / 1024);
	seq_printf(file_proc, "\"memoria_buffer\":%lu,\n", mem_buffer / 1024);
	seq_printf(file_proc, "\"memoria_shared\":%lu,\n", mem_shared / 1024);
	seq_printf(file_proc, "\"memoria_freehi\":%lu,\n", mem_freehi / 1024);
    seq_printf(file_proc, "\"memoria_consumida\":%lu,\n", mem_u / 1024);
    seq_printf(file_proc, "\"memoria_utilizada\":%lu\n}", (mem_u * 100)/mem_total) ;

    return 0;
}

static int abrir_aproc(struct inode *inode, struct  file *file) {
  return single_open(file, escribir_a_proc, NULL);
}

static struct file_operations archivo_operaciones =
{    
    .open = abrir_aproc,
    .read = seq_read
};


static int __init memo_grupo12_init(void)
{
    proc_create("memo_grupo12", 0, NULL, &archivo_operaciones);
    printk(KERN_INFO "Grupo 12\n");

    return 0;
}

static void __exit memo_grupo12_cleanup(void)
{
    remove_proc_entry("memo_grupo12", NULL);
    printk(KERN_INFO "Sistemas Operativos 1 Primer Semestre 2021\n");
}
module_init(memo_grupo12_init);
module_exit(memo_grupo12_cleanup); 